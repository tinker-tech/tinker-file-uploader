/**
 * Returns sum of two number
 * @param a number
 * @param b number
 * @return a + b
 */
export default (a: number, b: number) => a + b;
