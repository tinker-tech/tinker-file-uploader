import sum from "./examples/modern";
import AWS from "aws-sdk";
import fs from "fs";
import { promisify } from "util";

const readFile = promisify(fs.readFile);

export default {
  /**
   * S3 uploader
   */
  s3: {
    bucketName: "",
    /**
     * Initializes file uploader
     * @param bucketName S3 bucket name
     */
    init(
      bucketName: string,
      config: any
    ) {
      this.bucketName = bucketName;
      AWS.config.update(config);
    },
    /**
     * Uploads file to s3
     * @param file File object
     * @param format File format. Default: pdf
     * @return URL to file
     */
    async upload(file: Express.Multer.File, format: string = "pdf"): Promise<string> {
      if (!file) {
        throw new Error("File not found");
      }

      const s3 = new AWS.S3();
      const filename = `${new Date().getTime()}.${format}`;

      const data = await readFile(file.path);
      const params = {
        Bucket: this.bucketName,
        Key: filename,
        Body: data,
        ACL: "public-read",
      };
      const response: any = await s3.upload(params).promise();

      return Promise.resolve(response.Location);
    },
    async uploadBuffer(buffer: Buffer, format: string = "pdf"): Promise<string> {
      if (!buffer) {
        throw new Error("Buffer not found");
      }

      const s3 = new AWS.S3();
      const filename = `${new Date().getTime()}.${format}`;

      const params = {
        Bucket: this.bucketName,
        Key: filename,
        Body: buffer,
        ACL: "public-read",
      };
      const response: any = await s3.upload(params).promise();

      return Promise.resolve(response.Location);
    }
  }
}
