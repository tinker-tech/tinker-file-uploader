"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Returns sum of two number
 * @param a number
 * @param b number
 * @return a + b
 */
exports.default = (a, b) => a + b;
//# sourceMappingURL=modern.js.map