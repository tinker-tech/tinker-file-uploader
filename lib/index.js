"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const aws_sdk_1 = __importDefault(require("aws-sdk"));
const fs_1 = __importDefault(require("fs"));
const util_1 = require("util");
const readFile = util_1.promisify(fs_1.default.readFile);
exports.default = {
    /**
     * S3 uploader
     */
    s3: {
        bucketName: "",
        /**
         * Initializes file uploader
         * @param bucketName S3 bucket name
         */
        init(bucketName, config) {
            this.bucketName = bucketName;
            aws_sdk_1.default.config.update(config);
        },
        /**
         * Uploads file to s3
         * @param file File object
         * @param format File format. Default: pdf
         * @return URL to file
         */
        upload(file, format = "pdf") {
            return __awaiter(this, void 0, void 0, function* () {
                if (!file) {
                    throw new Error("File not found");
                }
                const s3 = new aws_sdk_1.default.S3();
                const filename = `${new Date().getTime()}.${format}`;
                const data = yield readFile(file.path);
                const params = {
                    Bucket: this.bucketName,
                    Key: filename,
                    Body: data,
                    ACL: "public-read",
                };
                const response = yield s3.upload(params).promise();
                return Promise.resolve(response.Location);
            });
        },
        uploadBuffer(buffer, format = "pdf") {
            return __awaiter(this, void 0, void 0, function* () {
                if (!buffer) {
                    throw new Error("Buffer not found");
                }
                const s3 = new aws_sdk_1.default.S3();
                const filename = `${new Date().getTime()}.${format}`;
                const params = {
                    Bucket: this.bucketName,
                    Key: filename,
                    Body: buffer,
                    ACL: "public-read",
                };
                const response = yield s3.upload(params).promise();
                return Promise.resolve(response.Location);
            });
        }
    }
};
//# sourceMappingURL=index.js.map